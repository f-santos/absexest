glm_sex_estimation <- function(training, test, target, threshold = 0.70) {

    ## 1. Keep only useful columns in training and test data:
    training <- training[, c("Sexe", colnames(target))]
    test <- test[, c("Sexe", colnames(target))]

    ## 2. Train the full model and compute accuracy in LOOCV:
    mod <- glm(Sexe ~ ., data = training, family = "binomial")
    tab.cv <- matrix(NA, nrow = nrow(training), ncol = 3)
    colnames(tab.cv) <- c("ProbM", "Sex", "Estimate")
    tab.cv <- as.data.frame(tab.cv)
    tab.cv[, "Sex"] <- training$Sexe

    for (i in seq_len(nrow(training))) {
        temp <- training[-i, ]
        targ <- training[i, ]
        mod.cv <- glm(Sexe ~ ., data = temp, family = "binomial")
        tab.cv[i, "ProbM"] <- predict(mod.cv,
                                      newdata = targ[, -1, drop = FALSE],
                                      type = "response")
        tab.cv[i, "Estimate"] <- sex_estimate(tab.cv[i, "ProbM"],
                                              threshold = threshold)
    }
    train.confusion <- table(Sex = tab.cv$Sex, Prediction = tab.cv$Estimate) |>
        as.data.frame.matrix()
    train.indet <- round(100 * sum(tab.cv$Estimate == "I") / nrow(tab.cv), 2)
    deter <- subset(tab.cv, Estimate != "I")
    train.error <- round(100 * sum(deter[, 2] != deter[, 3]) / nrow(deter), 2)

    ## 3. Predictions on the test sample:
    test.probs <- predict(mod, type = "response", newdata = test)
    test.preds <- sapply(X = test.probs,
                         FUN = sex_estimate,
                         threshold = threshold)
    test.confusion <- table(Sex = test$Sex, Prediction = test.preds) |>
        as.data.frame.matrix()
    test.indet <- round(100 * sum(test.preds == "I") / length(test.preds), 2)
    deter <- data.frame(Sex = test$Sexe, Estimate = test.preds) |>
        subset(Estimate != "I")
    test.error <- round(100 * sum(deter[, 1] != deter[, 2]) / nrow(deter), 2)

    ## 4. Sex estimation of the target individual:
    target.prob <- predict(mod, type = "response", newdata = target)
    target.sex <- sex_estimate(target.prob, threshold)

    ## Return results in a list:
    return(list(
        train.confusion = train.confusion,
        train.indet = train.indet,
        train.error = train.error,
        test.confusion = test.confusion,
        test.indet = test.indet,
        test.error = test.error,
        target.prob = target.prob,
        target.sex = target.sex
    ))
}

BASE: BAse of skull Sex Estimation
==================================

Texte de présentation...

## Installation of the R package from GitLab

### Install prerequisites

1. Make sure that [Git](https://git-scm.com/) and a [recent version of R](https://cran.r-project.org/) (newer than 4.1.0) are installed.

2. Install the R package `remotes` by typing the following command line into the R console:

   ```r
   install.packages("remotes")
   ```

3. Install build environment:
    * **Linux**: no additional operation required.
    * **OSX**: install *[XCODE](https://developer.apple.com/xcode/)*.
    * **Windows**: install the latest version of *[Rtools](https://cran.r-project.org/bin/windows/Rtools/)*.

### Install the package

Run the following command in R:

```r
remotes::install_git('https://gitlab.com/f-santos/base.git')
```

## Run the GUI

To start the graphical interface, simply type the following commands into the R console:

```r
library(BASE)
start_base()
```

## License

This package is available under a [CeCILL 2.1 free software license](http://cecill.info/licences/Licence_CeCILL_V2.1-en.html).

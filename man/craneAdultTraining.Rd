\name{craneAdultTraining}
\alias{craneAdultTraining}
\docType{data}
\title{
  Dataframe for training models AD1 and AD9-13.
}
\description{
  This dataframe includes 372 complete individuals for the variables
  used in models AD1 and AD9-13.
}
\usage{data("craneAdultTraining")}
\references{
  Boucherie, A. (2023) Analyse du dimorphisme sexuel de variables
  métriques de la base du crâne : intérêts archéo-anthropologiques et
  forensiques. PhD Thesis. Université Libre de Bruxelles.
}
\examples{
data(craneAdultTraining)
}
\keyword{datasets}

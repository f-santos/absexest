\name{baseAdultTest}
\alias{baseAdultTest}
\docType{data}
\title{
  Dataframe for testing the accuracy of model AD8.
}
\description{
  This dataframe includes 100 complete individuals for the 6 variables
  of model AD8.
}
\usage{data("baseAdultTraining")}
\references{
  Boucherie, A. (2023) Analyse du dimorphisme sexuel de variables
  métriques de la base du crâne : intérêts archéo-anthropologiques et
  forensiques. PhD Thesis. Université Libre de Bruxelles.
}
\examples{
data(baseAdultTraining)
}
\keyword{datasets}
